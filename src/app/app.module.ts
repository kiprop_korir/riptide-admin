import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import {AddVenuePage} from "../pages/add-venue/add-venue";
import {HttpModule} from "@angular/http";
import {HttpProvider} from "../provider/http/httpProvider";
import {IonicStorageModule} from "@ionic/storage";
import {ToastProvider} from "../provider/toastProvider";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";
import {VenueDetailPage} from "../pages/venue-detail/venue-detail";
import {ScreenDetailPage} from "../pages/screen-detail/screen-detail";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
      LoginPage,
      AddVenuePage,
      VenueDetailPage,
      ScreenDetailPage
  ],
  imports: [
    BrowserModule,
      HttpModule,
      IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
      LoginPage,
      AddVenuePage,
      VenueDetailPage,
      ScreenDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
      HttpProvider,
      ToastProvider,
      UniqueDeviceID,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
