import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ScreenDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-screen-detail',
  templateUrl: 'screen-detail.html',
})
export class ScreenDetailPage {

  screenData:any;
    screenName:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.screenData = navParams.get('screenData');
    this.screenName = navParams.get('screenName');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScreenDetailPage');
  }

}
