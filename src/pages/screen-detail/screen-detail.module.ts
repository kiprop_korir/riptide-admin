import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScreenDetailPage } from './screen-detail';

@NgModule({
  declarations: [
    ScreenDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ScreenDetailPage),
  ],
})
export class ScreenDetailPageModule {}
