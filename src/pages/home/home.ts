import { Component } from '@angular/core';
import {NavController, ModalController, NavParams} from 'ionic-angular';
import {AddVenuePage} from "../add-venue/add-venue";
import {VenueDetailPage} from "../venue-detail/venue-detail";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    venues:any;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public navContoller:NavController) {

      this.venues = this.navParams.get('venueData');

  }

    addNewVenue() {
        const modal = this.modalCtrl.create(AddVenuePage);
        modal.present();
    }

    selectedVenue()
    {
        this.navContoller.push(VenueDetailPage,{'venueId':1})
    }
}
