import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {HttpProvider} from "../../provider/http/httpProvider";
import {Storage} from "@ionic/storage";
import {ToastProvider} from "../../provider/toastProvider";
import {ScreenDetailPage} from "../screen-detail/screen-detail";

@IonicPage()
@Component({
  selector: 'page-venue-detail',
  templateUrl: 'venue-detail.html',
})
export class VenueDetailPage {

  venueId:any;
  ScreenData:any;
  loader:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http:HttpProvider,
              public storage: Storage,
              public toast:ToastProvider,
              public loading:LoadingController,
              public navController: NavController) {
    this.venueId = this.navParams.get('venueId');
    let url = "/Screens";
    let jsonData:any = {};

    this.loader= this.loading.create({content:'Loading'});
      this.loader.present();
      this.storage.get('user').then(data=>{
        jsonData.userId = data.userId;

          this.storage.get('token').then(data=>{

              jsonData.token = data;

              jsonData.venueId = this.venueId;

              this.http.post(url,jsonData).then((data)=>{

                this.loader.dismiss();

                  let body = JSON.parse(data['_body']);

                  if(body['responseCode'] == '200')
                  {

                      console.info(body);
                      console.info(body['ScreenData']);
                      this.ScreenData = body['ScreenData'];
                  }


              },(err)=>{
                  toast.showMessage('Error loading screens. Try again.');
                  this.loader.dismiss();
              });
          });
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VenueDetailPage');
  }

    manageScreen() {

        let jsonData: any = {};

        this.storage.get('user').then(user => {
            jsonData.userId = user.userId;
            this.storage.get('token').then(token => {
                jsonData.token = token;
                jsonData.venueId = this.venueId;
                jsonData.screenId = 5;
                return new Promise((resolve, reject) => {

                    this.http.post('/Assets', jsonData).then(data => {

                        let body = JSON.parse(data['_body']);

                        if(body['responseCode'] == '200') {

                            this.navController.push(ScreenDetailPage,{'screenData':body['ScreenData'],'screenName':'somename'})

                        }

                        }, err => {

                        console.error(err);

                    })

                })
            })

        });


    }
}
