import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpProvider} from "../../provider/http/httpProvider";
import {LoadingController} from "ionic-angular";
import {HomePage} from "../home/home";
import {Storage} from "@ionic/storage";
import {ToastProvider} from "../../provider/toastProvider";

import {UniqueDeviceID} from "@ionic-native/unique-device-id";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user:any;
  loading:any;
  deviceId:any

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public httpProvider:HttpProvider,
              public loader:LoadingController,
              public storage:Storage,
              public message: ToastProvider,
              public uniqueDeviceId: UniqueDeviceID) {
    this.user = new FormGroup({
        username: new FormControl('korir254@gmail.com',Validators.required),
        password: new FormControl('123Qwaszx',Validators.required)
    });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
    doLogin(user)
    {

        //get device id
        this.uniqueDeviceId.get().then((data)=>{
            this.deviceId=data;
        }).catch((err)=>{

        });


        let url = "/Login";
        let jsonData: any = {};
        jsonData.emailAddress = this.user.getRawValue().username;
        jsonData.password = this.user.getRawValue().password;
        // jsonData.venDeviceId = this.device.uuid; // not test code
        jsonData.deviceId = this.deviceId;  // test code


        this.loading = this.loader.create({content:'Signing in...'});

        return new Promise((resolve,reject)=>{
            this.httpProvider.post(url,jsonData)
                .then(data=>{

                    let body = JSON.parse(data['_body']);

                    if(body['responseCode'] == '200')
                    {

                        console.info(body);
                        console.info(body['venueData']);

                        this.storage.set('user',body['userData']);
                        this.storage.set('token',body['token']);


                        this.message.showMessage('Successfully logged in!');

                        this.navCtrl.setRoot(HomePage,{'venueData':body['venueData']});

                    }else {
                        this.message.showMessage('Incorrect username or password');
                        this.loading.dismiss();
                    }

                    resolve(data);
                },err=>{

                    this.message.showMessage('Incorrect username or password');
                    this.loading.dismiss();

                    reject(err);
                }).catch((err)=>{

                this.message.showMessage('Incorrect username or password');
                this.loading.dismiss();
            })
        });

        // this.http.post(url, jsonData).then((data:any) => {
       // this.navCtrl.setRoot(HomePage);
    }
    forgotPassword()
    {
      alert('forgotPassword');
    }

}
