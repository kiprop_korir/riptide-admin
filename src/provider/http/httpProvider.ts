import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpProvider {
    BASE_URL      = "https://redcape.venuesmedia.com/Pondhoppers_Trident/";
    LOGIN           = this.BASE_URL + "Login";
    SLIDING_IMAGES  = this.BASE_URL + "GetScreens";
    MENU_PAGE_DATE  = this.BASE_URL + "Pages";
    STATUS          = this.BASE_URL + "GetStatus";

    constructor (
        public http: Http
    ) {

    }

    post(url, json) {
        console.log(JSON.stringify(json));
        return new Promise((resolve, reject) => {
            this.http.post("https://riptide.pondhoppers.co.uk/Riptide_Mobile_API"+url, JSON.stringify(json))
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    // console.log("error", JSON.stringify(err));
                    reject(err);
                });
        });
    }

    get(url) {
        console.log(JSON.stringify(url));
        return new Promise((resolve, reject) => {
            this.http.get(url)
                .subscribe(data => {
                    // console.log("success", JSON.stringify(data));
                    resolve(data);
                }, err => {
                    // console.log("error", JSON.stringify(err));
                    reject(err);
                });
        });
    }
}
